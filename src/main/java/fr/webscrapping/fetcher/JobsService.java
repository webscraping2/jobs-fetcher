package fr.webscrapping.fetcher;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import org.bson.Document;
import org.bson.conversions.Bson;

import io.vertx.core.json.JsonObject;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class JobsService {
    @Inject MongoClient mongoClient;

    public List<JsonObject> list(){
        return this.mapDocumentsToJson(this.getCollection().find().iterator());
    }

    public List<JsonObject> list(final int pageNumber, final int nbOfDocuments){
        return this.mapDocumentsToJson(this.getCollection().find()
            .skip(pageNumber)
            .limit(nbOfDocuments)
            .iterator());
    }

    public List<JsonObject> select(final Bson filter) {
        return this.mapDocumentsToJson(this.getCollection().find(filter).iterator());
    }

    public List<JsonObject> select(final Bson filter, final int pageNumber, final int nbOfDocuments) {
        return this.mapDocumentsToJson(this.getCollection().find(filter)
            .skip(pageNumber)
            .limit(nbOfDocuments)
            .iterator());
    }

    public long count() {
        return this.getCollection().countDocuments();
    }

    public long count(final Bson filter) {
        return this.getCollection().countDocuments(filter);
    }

    private List<JsonObject> mapDocumentsToJson(final MongoCursor<Document> cursor) {
        final List<JsonObject> list = new ArrayList<>();

        try {
            while (cursor.hasNext()) {
                Document document = cursor.next();
                
                final JsonObject docToJson = new JsonObject(document.toJson());
                docToJson.remove("_id");

                list.add(docToJson);
            }
        } finally {
            cursor.close();
        }

        return list;
    }

    private MongoCollection<Document> getCollection(){
        return mongoClient.getDatabase("jobs").getCollection("jobs");
    }
}