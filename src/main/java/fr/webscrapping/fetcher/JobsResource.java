package fr.webscrapping.fetcher;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.bson.Document;

import io.vertx.core.json.JsonObject;

@Path("/api/fetching")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public class JobsResource {
    @Inject JobsService service;

    @GET
    @Path("/list")
    public List<JsonObject> list(){
        return this.service.list();
    }

    @GET
    @Path("/list/{pageNumber}/{nbPerPage}")
    public List<JsonObject> list(@PathParam("pageNumber") final int pageNumber, @PathParam("nbPerPage") final int nbPerPage){
        return this.service.list(pageNumber, nbPerPage);
    }

    @GET
    @Path("/select")
    public List<JsonObject> select(final Document filter) {
        return this.service.select(filter);
    }

    @GET
    @Path("/select/{pageNumber}/{nbPerPage}")
    public List<JsonObject> select(final Document filter, @PathParam("pageNumber") final int pageNumber, @PathParam("nbPerPage") final int nbPerPage) {
        return this.service.select(filter, pageNumber, nbPerPage);
    }

    @GET
    @Path("/count/all")
    public long count() {
        return this.service.count();
    }

    @GET()
    @Path("/count")
    public long count(final Document filter) {
        return this.service.count(filter); 
    }
}